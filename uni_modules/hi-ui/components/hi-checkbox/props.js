/**
 * 组件属性
 *
 * @author 济南晨霜信息技术有限公司
 * @mobile 18560000860 / 18754137913
 */
export default {
    // hover class
    hover: {
        type: String,
        default: "hi-hover"
    },

    // 是否禁用
    disabled: {
        type: Boolean,
        default: false
    },

    // 使用 v-model 双向绑定值，单独使用时需要使用 v-model 绑定复选框的选中状态
    modelValue: {
        type: Boolean,
        default: false
    },

    // 复选框的值，在 hi-checkbox-group 组件中使用时需要绑定此值
    value: {
        type: [String, Number],
        default: undefined
    },

    // 是否选中，主要用于处理头条小程序 provide/inject 不生效的问题
    checked: {
        type: Boolean,
        default: false
    },

    // 复选框的 label
    label: {
        type: String,
        default: undefined
    },

    // 文本颜色
    labelColor: {
        type: String,
        default: ""
    },

    // 激活时文本颜色
    activeLabelColor: {
        type: String,
        default: ""
    },

    // 文本大小
    labelFontSize: {
        type: String,
        default: ""
    },

    // 是否单独使用
    alone: {
        type: Boolean,
        default: false
    },

    // 图标名称
    icon: {
        type: String,
        default: "__checked"
    },

    // 图标颜色
    iconColor: {
        type: String,
        default: ""
    },

    // 图标大小
    iconSize: {
        type: String,
        default: ""
    },

    // 是否异步变更
    async: {
        type: Boolean,
        default: false
    },

    // 框框的大小
    size: {
        type: String,
        default: ""
    },

    // 框框未激活时的颜色
    inactiveColor: {
        type: String,
        default: ""
    },

    // 框框激活时的颜色
    activeColor: {
        type: String,
        default: ""
    },

    // 是否显示边框
    border: {
        type: Boolean,
        default: false
    },

    // 边框宽度
    borderWidth: {
        type: String,
        default: ""
    },

    // 边框未激活时的颜色
    inactiveBorderColor: {
        type: String,
        default: ""
    },

    // 边框激活时的颜色
    activeBorderColor: {
        type: String,
        default: ""
    },

    // 框框的圆角值
    radius: {
        type: String,
        default: ""
    },

    // 圆形
    circle: {
        type: Boolean,
        default: false
    },

    // 主题
    theme: {
        type: String,
        default: ""
    },

    // 镂空？
    plain: {
        type: Boolean,
        default: false
    },

    // 浅化
    tint: {
        type: Boolean,
        default: false
    },

    // 浅化透明度
    tintOpacity: {
        type: [String, Number],
        default: ""
    }
};
