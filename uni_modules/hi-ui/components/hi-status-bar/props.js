/**
 * 组件属性
 *
 * @author 济南晨霜信息技术有限公司
 * @mobile 18560000860 / 18754137913
 */
export default {
    // 高度
    height: {
        type: String,
        default: ""
    },

    // 背景
    bg: {
        type: String,
        default: ""
    },

    // 是否自动应用在 pages.json 中配置的背景
    autoBg: {
        type: Boolean,
        default: true
    }
};
