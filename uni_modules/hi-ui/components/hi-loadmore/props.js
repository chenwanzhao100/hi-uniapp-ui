/**
 * 组件属性
 *
 * @author 济南晨霜信息技术有限公司
 * @mobile 18560000860 / 18754137913
 */
export default {
    // hover-class
    hover: {
        type: String,
        default: "hi-hover"
    },

    // 状态。loadmore: 加载更多，loading: 加载中，nomore: 没有更多
    status: {
        type: String,
        default: ""
    },

    // loading 图标名称
    icon: {
        type: String,
        default: "__loading"
    },

    // loadmore 文字
    more: {
        type: String,
        default: "加载更多"
    },

    // loading 文字
    loading: {
        type: String,
        default: "正在加载..."
    },

    // nomore 文字
    nomore: {
        type: String,
        default: " — 没有更多了 — "
    },

    // 文字颜色
    color: {
        type: String,
        default: ""
    },

    // 文字大小
    fontSize: {
        type: String,
        default: ""
    },

    // 是否纵向
    vertical: {
        type: Boolean,
        default: false
    }
};
