/**
 * 组件属性
 *
 * @author 济南晨霜信息技术有限公司
 * @mobile 18560000860 / 18754137913
 */
export default {
    // 格式化字符串
    // 例如 “距离倒计时结束还剩{WW}周{DD | D}天{HH | D}时{MM | M}分{SS | S}秒{MSS | MS}毫秒”，注意，字母大写，双字母（毫秒三字母）表示补零，单字母（毫秒双字母）表示不补零
    format: {
        type: String,
        default: "{DD}天{HH}时{MM}分{SS}秒"
    },

    // 间隔多久更新一次
    interval: {
        type: [Number, String],
        default: 1000
    },

    // 倒计时结束后的文本
    tips: {
        type: String,
        default: ""
    },

    // 结束日期
    date: {
        type: [Date, Number, String],
        default: undefined
    },

    // 颜色
    color: {
        type: String,
        default: ""
    },

    // 大小
    fontSize: {
        type: String,
        default: ""
    },

    // 粗细
    fontWeight: {
        type: String,
        default: ""
    },

    // 间距
    gap: {
        type: String,
        default: ""
    },

    // 主题
    theme: {
        type: String,
        default: ""
    },

    // 镂空？
    plain: {
        type: Boolean,
        default: false
    },

    // 浅化
    tint: {
        type: Boolean,
        default: false
    },

    // 浅化透明度
    tintOpacity: {
        type: [String, Number],
        default: ""
    },

    // 背景
    bg: {
        type: String,
        default: ""
    },

    // 圆角
    radius: {
        type: String,
        default: ""
    },

    // 边框
    border: {
        type: Boolean,
        default: false
    },

    // 内间距
    padding: {
        type: String,
        default: ""
    },

    // 数字大小
    numFontSize: {
        type: String,
        default: ""
    },

    // 数字颜色
    numColor: {
        type: String,
        default: ""
    },

    // 数字粗细
    numFontWeight: {
        type: String,
        default: ""
    },

    // 数字宽度
    numWidth: {
        type: String,
        default: ""
    },

    // 数字高度
    numHeight: {
        type: String,
        default: ""
    },

    // 数字圆角
    numRadius: {
        type: String,
        default: ""
    },

    // 数字边框
    numBorder: {
        type: Boolean,
        default: false
    },

    // 数字边框粗细
    numBorderWidth: {
        type: String,
        default: ""
    },

    // 数字边框颜色
    numBorderColor: {
        type: String,
        default: ""
    },

    // 数字边框样式
    numBorderStyle: {
        type: String,
        default: ""
    },

    // 数字内边距
    numPadding: {
        type: String,
        default: ""
    },

    // 主题
    numTheme: {
        type: String,
        default: ""
    },

    // 镂空？
    numPlain: {
        type: Boolean,
        default: false
    },

    // 浅化
    numTint: {
        type: Boolean,
        default: false
    },

    // 浅化透明度
    numTintOpacity: {
        type: [String, Number],
        default: ""
    }
};
