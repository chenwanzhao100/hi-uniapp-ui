/**
 * 组件属性
 *
 * @author 济南晨霜信息技术有限公司
 * @mobile 18560000860 / 18754137913
 */
export default {
    // 列表
    list: {
        type: Array,
        default: () => []
    },

    // 头像图片属性的 keyName
    keyName: {
        type: String,
        default: "avatar"
    },

    // 展示的数量
    count: {
        type: Number,
        default: 5
    },

    // 是否开启动画
    animation: {
        type: Boolean,
        default: false
    },

    // 动画间隔时间，毫秒
    delay: {
        type: [Number, String],
        default: 3000
    },

    // 图片的裁剪模式
    mode: {
        type: String,
        default: "scaleToFill"
    },

    // 是否显示更多按钮
    more: {
        type: Boolean,
        default: false
    },

    // 防抖动时间，毫秒
    time: {
        type: [Number, String],
        default: 0
    },

    // 更多按钮图标名称
    moreIcon: {
        type: String,
        default: "__gengduo"
    },

    // 头像大小
    size: {
        type: String,
        default: ""
    },

    // 头像偏移量
    offset: {
        type: String,
        default: ""
    },

    // item 边框
    itemBorder: {
        type: Boolean,
        default: false
    },

    // item 边框颜色
    itemBorderColor: {
        type: String,
        default: ""
    },

    // item 圆角
    itemRadius: {
        type: String,
        default: ""
    },

    // item 背景
    itemBg: {
        type: String,
        default: ""
    },

    // 更多按钮边框
    moreBorder: {
        type: Boolean,
        default: false
    },

    // 更多按钮边框颜色
    moreBorderColor: {
        type: String,
        default: ""
    },

    // 更多按钮圆角
    moreRadius: {
        type: String,
        default: ""
    },

    // 更多按钮文字颜色
    moreColor: {
        type: String,
        default: ""
    },

    // 更多按钮文字大小
    moreFontSize: {
        type: String,
        default: ""
    },

    // 更多按钮背景
    moreBg: {
        type: String,
        default: ""
    }
};
