/**
 * 组件属性
 *
 * @author 济南晨霜信息技术有限公司
 * @mobile 18560000860 / 18754137913
 */
export default {
    // 列数
    cols: {
        type: [Number, String],
        default: ""
    },

    gap: {
        type: String,
        default: ""
    }
};
