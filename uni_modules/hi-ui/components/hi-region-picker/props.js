/**
 * 组件属性
 *
 * @author 济南晨霜信息技术有限公司
 * @mobile 18560000860 / 18754137913
 */
export default {
    // 显示状态
    show: {
        type: Boolean,
        default: false
    },

    // 选中项下标
    values: {
        type: Array,
        default: () => [0, 0, 0]
    },

    // 省市区完整数据
    region: {
        type: Array,
        default: () => []
    },

    // 省数据
    provinces: {
        type: Array,
        default: () => []
    },

    // 市数据
    cities: {
        type: Array,
        default: () => []
    },

    // 区数据
    areas: {
        type: Array,
        default: () => []
    },

    // 显示字段的 keyName
    keyName: {
        type: String,
        default: "name"
    },

    // item 的高度
    itemHeight: {
        type: [String],
        default: ""
    },

    // item 文字大小
    itemFontSize: {
        type: String,
        default: ""
    },

    // item 文字颜色
    itemColor: {
        type: String,
        default: ""
    },

    // 点击遮罩是否关闭
    closeOnClickOverlay: {
        type: Boolean,
        default: true
    },

    // 是否显示loading
    loading: {
        type: Boolean,
        default: false
    },

    // 标题
    title: {
        type: String,
        default: ""
    },

    // 标题颜色
    titleColor: {
        type: String,
        default: ""
    },

    // 标题文字大小
    titleFontSize: {
        type: String,
        default: ""
    },

    // 标题文字粗细
    titleFontWeight: {
        type: String,
        default: ""
    },

    // 切换后是否子项下标归零
    returnZero: {
        type: Boolean,
        default: false
    },

    // 取消按钮文本
    cancelText: {
        type: String,
        default: "取消"
    },

    // 取消按钮文字颜色
    cancelColor: {
        type: String,
        default: ""
    },

    // 取消按钮文字大小
    cancelFontSize: {
        type: String,
        default: ""
    },

    // 确认按钮文本
    confirmText: {
        type: String,
        default: "确认"
    },

    // 确认按钮文字颜色
    confirmColor: {
        type: String,
        default: ""
    },

    // 确认按钮文字大小
    confirmFontSize: {
        type: String,
        default: ""
    },

    // 背景
    bg: {
        type: String,
        default: ""
    },

    // 圆角
    radius: {
        type: String,
        default: ""
    },

    // 高度
    height: {
        type: String,
        default: ""
    },

    // 最大高度
    maxHeight: {
        type: String,
        default: ""
    },

    // 是否显示遮罩
    mask: {
        type: Boolean,
        default: true
    },

    // 遮罩背景
    maskBg: {
        type: String,
        default: ""
    },

    // 遮罩配置
    maskOpts: {
        type: Object,
        default: undefined
    },

    // loading 配置
    loadingOpts: {
        type: Object,
        default: undefined
    }
};
