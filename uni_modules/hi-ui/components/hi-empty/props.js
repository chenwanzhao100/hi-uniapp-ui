/**
 * 组件属性
 *
 * @author 济南晨霜信息技术有限公司
 * @mobile 18560000860 / 18754137913
 */
export default {
    // 图标名称
    icon: {
        type: String,
        default: "__kongshuju"
    },

    // 图标裁剪模式
    mode: {
        type: String,
        default: ""
    },

    // 提示文字
    tips: {
        type: String,
        default: "Ops! 暂无数据~"
    },

    // 是否显示按钮
    showBtn: {
        type: Boolean,
        default: false
    },

    // 按钮文字
    btnText: {
        type: String,
        default: "继续逛逛"
    },

    // 按钮主题
    btnTheme: {
        type: String,
        default: "primary"
    },

    // 按钮其他配置
    btnOpts: {
        type: Object,
        default: () => ({})
    },

    // 按钮距离上方元素的距离
    btnTop: {
        type: String,
        default: ""
    },

    // 按钮宽度
    btnWidth: {
        type: String,
        default: "auto"
    },

    // 按钮高度
    btnHeight: {
        type: String,
        default: "30px"
    },

    // 按钮背景
    btnBg: {
        type: String,
        default: ""
    },

    // 按钮文字颜色
    btnColor: {
        type: String,
        default: ""
    },

    // 按钮文字大小
    btnFontSize: {
        type: String,
        default: "12px"
    },

    // 按钮圆角
    btnRadius: {
        type: String,
        default: "3px"
    },

    // 高度
    height: {
        type: String,
        default: ""
    },

    // 背景
    bg: {
        type: String,
        default: ""
    },

    // 圆角
    radius: {
        type: String,
        default: ""
    },

    // 文字大小
    fontSize: {
        type: String,
        default: ""
    },

    // 文字颜色
    color: {
        type: String,
        default: ""
    },

    // 图标颜色
    iconColor: {
        type: String,
        default: ""
    },

    // 图标大小
    iconFontSize: {
        type: String,
        default: ""
    }
};
