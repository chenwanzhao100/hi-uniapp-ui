/**
 * @descript URL 处理工具函数集
 *
 * @author 济南晨霜信息技术有限公司
 * @mobile 18560000860 / 18754137913
 *
 * @list 函数列表
 * ========================================================================================================================
 * = urlCombine              : 合并基准 URL 和相对 URL 成一个完整的 URL
 * = urlGetMainPart          : 获取 URL 问号之前的部分
 * = urlGetParamsObject      : 将 URL 字符串中的参数解析为对象
 * = urlParamsObjectToString : 将对象转换为 URL 参数字符串
 * = urlAppendParams         : 将参数追加到 URL 中
 * ========================================================================================================================
 */
import { isString, isObject, isAbsoluteURL } from "./validate";

/**
 * 合并基准 URL 和相对 URL 成一个完整的 URL
 * @param {String} baseURL 基准 URL
 * @param {String} relativeURL 相对 URL
 * @returns {String} 返回合并后的完整 URL
 */
export const urlCombine = (baseURL = "", relativeURL = "") => {
    if (isAbsoluteURL(relativeURL)) return relativeURL;
    if (relativeURL && isString(relativeURL) && isString(baseURL)) {
        return baseURL.replace(/\/+$/, "") + "/" + relativeURL.replace(/^\/+/, "");
    }
    return baseURL;
};

/**
 * 获取 URL 问号之前的部分
 * @param {String} url URL 字符串
 * @returns {String} 返回URL 主要部分（问号之前的部分）字符串
 */
export const urlGetMainPart = (url = "") => url.split("?")?.[0] || "";

/**
 * 将 URL 字符串中的参数解析为对象
 * @param {String} url URL 字符串
 * @returns {Object} 解析后的参数对象
 */
export const urlGetParamsObject = (url) => {
    const queryStartIndex = url.indexOf("?");
    if (queryStartIndex === -1) return {};
    const queryString = url.substring(queryStartIndex + 1);
    const params = {};
    const keyValuePairs = queryString.split("&");
    for (const pair of keyValuePairs) {
        const [key, value] = pair.split("=");
        if (key) params[key] = value || "";
    }
    return params;
};

/**
 * 将对象转换为 URL 参数字符串
 * @param {Object} obj 参数对象
 * @param {String} separator 是否拼接问号，默认：false
 * @returns {String} 参数字符串
 */
export const urlParamsObjectToString = (obj, separator = false) => {
    let params = [];
    for (let key in obj) {
        if (obj.hasOwnProperty(key)) params.push(`${encodeURIComponent(key)}=${encodeURIComponent(obj[key])}`);
    }
    if (params.length > 0) {
        if (separator) return `?${params.join("&")}`;
        else return params.join("&");
    }
    return "";
};

/**
 * 将参数追加到 URL 中
 * @param {String} url URL 字符串
 * @param {Object | String} params 参数对象或参数字符串
 * @returns {String} 追加参数后的 URL
 */
export const urlAppendParams = (url = "", params) => {
    let hasSeparator = url.includes("?");

    // 参数是字符串
    if (isString(params)) {
        if (hasSeparator) return `${url}&${params}`;
        else return `${url}?${params}`;
    }

    // 参数是对象
    if (isObject(params)) {
        const paramsStr = urlParamsObjectToString(params);
        if (hasSeparator) return `${url}&${paramsStr}`;
        else return `${url}?${paramsStr}`;
    }

    return url;
};
