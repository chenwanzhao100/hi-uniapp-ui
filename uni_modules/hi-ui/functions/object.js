/**
 * @descript Object 类型数据处理工具函数集
 *
 * @author 济南晨霜信息技术有限公司
 * @mobile 18560000860 / 18754137913
 *
 * @list 函数列表
 * ========================================================================================================================
 * = objectDeepMerge : 深度合并对象，不影响原对象
 * ========================================================================================================================
 */
import { isObject, isArray } from "./validate";

/**
 * 深度合并两个对象
 * @param {Object} object1 要合并的对象一
 * @param {Object} object2 要合并的对象二
 * @returns {Object} 合并后的新对象数据，不影响原对象
 */
export function objectDeepMerge(object1, object2) {
    let merged = Object.assign({}, object1);
    for (let key in object2) {
        if (object2.hasOwnProperty(key)) {
            if (isObject(object2[key]) && !isArray(object2[key])) {
                if (isObject(merged[key]) && !isArray(merged[key])) {
                    merged[key] = objectDeepMerge(merged[key], object2[key]);
                } else {
                    merged[key] = Object.assign({}, object2[key]);
                }
            } else {
                merged[key] = object2[key];
            }
        }
    }
    return merged;
}
