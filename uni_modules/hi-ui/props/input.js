/**
 * <input /> 组件公共属性
 *
 * @author 济南晨霜信息技术有限公司
 * @mobile 18560000860 / 18754137913
 */
export default {
    // placeholder
    placeholder: {
        type: String,
        default: ""
    },

    // placeholder-class
    placeholderClass: {
        type: String,
        default: ""
    },

    // placeholder-style
    placeholderStyle: {
        type: String,
        default: ""
    },

    // 最大输入长度，设置为 -1 的时候不限制最大长度
    maxlength: {
        type: Number,
        default: 140
    },

    // 是否禁用
    disabled: {
        type: Boolean,
        default: false
    },

    // 指定光标与键盘的距离，单位 px
    cursorSpacing: {
        type: Number,
        default: 0
    },

    // 获取焦点
    focus: {
        type: Boolean,
        default: false
    },

    // 点击键盘右下角按钮时是否保持键盘不收起
    confirmHold: {
        type: Boolean,
        default: false
    },

    // 指定focus时的光标位置
    cursor: {
        type: Number,
        default: undefined
    },

    // 光标颜色
    cursorColor: {
        type: String,
        default: ""
    },

    // 光标起始位置，自动聚集时有效，需与selection-end搭配使用
    selectionStart: {
        type: Number,
        default: -1
    },

    // 光标结束位置，自动聚集时有效，需与selection-start搭配使用
    selectionEnd: {
        type: Number,
        default: -1
    },

    // 键盘弹起时，是否自动上推页面
    adjustPosition: {
        type: Boolean,
        default: true
    },

    // 键盘收起时，是否自动失去焦点
    autoBlur: {
        type: Boolean,
        default: false
    },

    // 是否忽略组件内对文本合成系统事件的处理。为 false 时将触发 compositionstart、compositionend、compositionupdate 事件，且在文本合成期间会触发 input 事件
    ignoreCompositionEvent: {
        type: Boolean,
        default: true
    },

    // 强制 input 处于同层状态，默认 focus 时 input 会切到非同层状态 (仅在 iOS 下生效)
    alwaysEmbed: {
        type: Boolean,
        default: false
    },

    // focus时，点击页面的时候不收起键盘
    holdKeyboard: {
        type: Boolean,
        default: false
    },

    // 安全键盘加密公钥的路径，只支持包内路径
    safePasswordCertPath: {
        type: String,
        default: undefined
    },

    // 安全键盘输入密码长度
    safePasswordLength: {
        type: Number,
        default: undefined
    },

    // 安全键盘加密时间戳
    safePasswordTimeStamp: {
        type: Number,
        default: undefined
    },

    // 安全键盘加密盐值
    safePasswordNonce: {
        type: String,
        default: undefined
    },

    // 安全键盘计算 hash 盐值，若指定custom-hash 则无效
    safePasswordSalt: {
        type: String,
        default: undefined
    },

    // 安全键盘计算 hash 的算法表达式，如 md5(sha1('foo' + sha256(sm3(password + 'bar'))))
    safePasswordCustomHash: {
        type: String,
        default: undefined
    },

    // 当 type 为 number, digit, idcard 数字键盘是否随机排列
    randomNumber: {
        type: Boolean,
        default: false
    },

    // 是否为受控组件。为 true 时，value 内容会完全受 setData 控制
    controlled: {
        type: Boolean,
        default: false
    },

    // 是否强制使用系统键盘和 Web-view 创建的 input 元素。为 true 时，confirm-type、confirm-hold 可能失效
    alwaysSystem: {
        type: Boolean,
        default: false
    },

    // 是一个枚举属性，它提供了用户在编辑元素或其内容时可能输入的数据类型的提示。
    inputmode: {
        type: String,
        default: "text"
    }
};
